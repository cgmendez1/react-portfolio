import React from "react";
import {Container, Heading, Hero} from 'react-bulma-components';

const Footer = () =>{
	return (
		<Container className="is-transparent">
				<div className="has-text-centered">
					<a href="https://www.facebook.com/carlo.mendez.is.a.walrus"><i class="fab fa-facebook-square fa-3x"></i></a> 
					<a href="https://gitlab.com/cgmendez1"><i class="fab fa-gitlab fa-3x"></i></a>
					<a href="https://ph.linkedin.com/in/carlo-mendez-68027a18b"><i class="fab fa-linkedin fa-3x"></i></a>

				</div>
				<div className="has-text-centered is-transparent">COPYRIGHT &copy; 2012. FOR EDUCATIONAL PURPOSE ONLY.</div>
		</Container>
	)
};

export default Footer;