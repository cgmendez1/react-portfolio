import React, {useState} from "react";
import {Container, Columns, Hero, Heading} from 'react-bulma-components';
import Swal from 'sweetalert2';
import NavBar from './NavBar.js';
import Footer from './Footer.js';

const Contact = () =>{
    const[name,setName] = useState("John Doe");
    const[email,setEmail] = useState("email@email.com");
    const[message,setMessage] = useState("your websites are amazing");

    let formData = new FormData();
    formData.name = name;
    formData.email = email;
    formData.message = message;
    const nameInputHandler = e =>{
    console.log("input the name here");
    setName(e.target.value);
    };
    const emailInputHandler = e =>{
    console.log("input the email here");
    setEmail(e.target.value);
    };
    const messageChangeHandler = e =>{
    console.log("input the message here");
    setMessage(e.target.value);
    }

    console.log("name is " + name);
    console.log("email is " + email);
    console.log("message is " + message);

    const submitFormClickHandler =()=>{
        console.log("you are clicking the submit button.");
        //basic syntax
        //fetch(route,options)
        //note: PROBABLY because router.post in index.js of server folder does request using '/send', we PROBABLY needed to add /send at the end of fetch url
        fetch("https://testservernicarlo.herokuapp.com/send", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        })
            .then(res =>{
                // format the response object
                return res.json();
            })

            .then(res =>{
                if(res.data.message === "message_not_sent"){
                    // alert("email not sent");
                    Swal.fire({
                        icon: "error",
                        title: "email not sent",
                        text: "Something went wrong!"
                    });
                } else{
                    Swal.fire({
                        icon: "success",
                        title: "email sent",
                        text: "The developer will contact you in 24 hours"
                    });
                }
            })
    };
  return(
    <Hero color="black" size="fullheight" gradient>
        <NavBar/>
        <Hero.Head renderAs="header">
            <Heading size ={1}>
                <h1 className="heading-text has-text-centered">
                    Contact me!
                </h1>
            </Heading>
        </Hero.Head>
        <Hero.Body>
          <Container className="has-text-centered">
            {/*declare a row*/}
            <Columns>
                {/*left*/}
                <Columns.Column>
                    <form>
                        <div className="field">
                            <label for="name">
                                Name
                            </label>
                            <div className="control">
                                <input id="name" className="input" type="text" value={name} onChange={nameInputHandler}/>
                            </div>  
                        </div>
                        <div className="field">
                            <label for="email">
                                email
                            </label>
                            <div className="control">
                                <input id="email" className="input" type="text" value={email} onChange={emailInputHandler}/>
                            </div>  
                        </div>
                        <div className="field">
                            <label for="message">
                                Message
                            </label>
                            <div className="control">
                                <textarea id="message" className="textarea" placeholder="input message here"  value={message} onChange={messageChangeHandler}>
                                </textarea>
                            </div>
                            <button class="button is-dark is-fullwidth" type="button" onClick={submitFormClickHandler}>Send    
                            </button>   
                        </div>
                    </form>
                </Columns.Column>
                {/*right*/}
                <Columns.Column>
                <iframe id="gmap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1930.863542589121!2d121.03322019325746!3d14.557593174151279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c901e242f095%3A0xcbab77db1aac9063!2sENZO%20BUILDING!5e0!3m2!1sen!2sph!4v1579496797775!5m2!1sen!2sph" ></iframe>
                </Columns.Column>
            </Columns>
        </Container>
        </Hero.Body>
        <Hero.Footer>
        <Footer/>
        </Hero.Footer>
          
    </Hero>
        
    )
}

export default Contact