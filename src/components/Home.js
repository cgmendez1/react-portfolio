import React from "react";
import {Container, Heading, Hero} from 'react-bulma-components';
import NavBar from './NavBar.js';
import Footer from './Footer.js';

const Home = () =>{
  return(
    <Hero className="is-danger" size="fullheight" gradient>
        <NavBar/>
        <Hero.Body>
          <Container className="has-text-centered">
            <Heading size ={1}>
                <h1 className="heading-text">
                    People are Lazy
                </h1>
            </Heading>
            <Heading size={4}>
                I'm also lazy.
            </Heading>
            <Heading size={6}>
                My works are optimized for this lazy generation. 
            </Heading>

        </Container>
        </Hero.Body>
        <Hero.Footer>
        <Footer/>
        </Hero.Footer>
        
    </Hero>    
        
    )
}

export default Home;