import React, {useState} from 'react';

import { Navbar } from "react-bulma-components";

import { NavLink} from "react-router-dom";

const NavBar = (props) => {
	console.log(props);
	const [open, setOpen] = useState(false);

	const clickBurgerHandler=()=>{
		setOpen(!open)
	}

	return (
		<Navbar className="is-transparent" active={open}>
			<Navbar.Brand>
				<NavLink className="navbar-item" to="/">
					<img src="./images/portfolio-logo.png" alt="Carlo" width="50" height="70" />
				</NavLink>
				<Navbar.Burger active={open} onClick={clickBurgerHandler}/>
			</Navbar.Brand>

			<Navbar.Menu>
				<Navbar.Container position="end">
					<NavLink className="navbar-item" to="/about">About Me</NavLink>
					<NavLink className="navbar-item" to="/projects">Projects</NavLink>
					<NavLink className="navbar-item" to="/contact">Contact</NavLink>
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};


export default NavBar;