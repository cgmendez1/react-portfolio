import React from "react";
import {Container, Heading, Hero} from 'react-bulma-components';
import NavBar from './NavBar.js';
import Footer from './Footer.js';

const About = () =>{
  return(
    <Hero size="fullheight" gradient>
        <NavBar/>
        <Hero.Body>
          <Container className="has-text-centered">
            <Heading size ={1}>
                <div class="image-container">
                    <img class ="animated pulse placard" alt="kek" src="./images/carlo.jpg" size={8}/>
                </div>
                <h1 className="heading-text">
                    Carlo Mendez
                </h1>
            </Heading>
            <Heading size={4}>
                I am a budding Web Developer aiming to learn while polishing my skills. I like Walruses. Also collects exotic pets. I want to be one.
            </Heading>
            <Heading size={5}>
                I also keep exotic pets.
            </Heading>

        </Container>
        </Hero.Body>
        <Hero.Footer>
        <Footer/>
        </Hero.Footer>
    </Hero>    
        
    )
}

export default About;
