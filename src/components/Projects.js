import React from "react"
import {Container, Heading, Hero} from 'react-bulma-components';
import NavBar from './NavBar.js';
import Footer from './Footer.js';

const Projects = () =>{
  return(
    <Hero className="is-danger" size="fullheight" gradient>
    <NavBar/>
        <Hero.Body>
          <Container className="has-text-centered">
            <Heading size ={1}>
                <h1 className="heading-text">
                    Projects
                </h1>
            </Heading>

            <Heading size={6}>
                Check out my previous projects.
            </Heading>
            <div>
                <a target="_blank" rel="noopener noreferrer"  href="https://cgmendez1.gitlab.io/kek/"><img class ="animated pulse placard" alt="kek" src="./images/kek-card.png" size={8} /></a>
            </div>
            <div>
                <a target="_blank" rel="noopener noreferrer"  href="http://afternoon-peak-79390.herokuapp.com/"><img class ="animated pulse placard" alt="kek" src="./images/envenomd-card.png" size={8}/></a>
            </div>
            <div>
                <a target="_blank" rel="noopener noreferrer" href="http://capstone-3-app.herokuapp.com/"><img class ="animated pulse placard" alt="book" src="./images/heroes-card.png" size={8}/></a>
            </div>
            
            
        </Container>
        </Hero.Body>
        <Hero.Footer>
        <Footer/>
        </Hero.Footer>
          
    </Hero>
        
    )
}

export default Projects